/*
 * Copyright (C) 2015 The CyanogenMod Project
 *               2017-2019 The LineageOS Project
 *               2020-2022 Paranoid Android
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.typeblog.lunatic.Utils;

import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerExemptionManager;
import android.os.UserHandle;
import android.util.Log;

import net.typeblog.lunatic.Constants.Constants;
import net.typeblog.lunatic.Manager.AnimationManager;
import net.typeblog.lunatic.Manager.SettingsManager;
import net.typeblog.lunatic.Services.CallReceiverService;
import net.typeblog.lunatic.Services.ChargingService;
import net.typeblog.lunatic.Services.FlashlightService;
import net.typeblog.lunatic.Services.MusicService;
import net.typeblog.lunatic.Services.NotificationService;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public final class ServiceUtils {

    private static final String TAG = "SpotlightServiceUtils";
    private static final boolean DEBUG = true;

    // Use reflection hack because Android Studio does not use our overridden framework.jar by default
    private static void startServiceAsCurrentUser(Context context, Intent intent) {
        try {
            final Field currentField = UserHandle.class.getField("CURRENT");
            UserHandle current = (UserHandle) currentField.get(null);
            final Method startServiceAsUser = Context.class.getMethod("startServiceAsUser", Intent.class, UserHandle.class);
            startServiceAsUser.invoke(context, intent, current);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void stopServiceAsCurrentUser(Context context, Intent intent) {
        try {
            final Field currentField = UserHandle.class.getField("CURRENT");
            UserHandle current = (UserHandle) currentField.get(null);
            final Method stopServiceAsUser = Context.class.getMethod("stopServiceAsUser", Intent.class, UserHandle.class);
            stopServiceAsUser.invoke(context, intent, current);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void startCallReceiverService(Context context) {
        if (DEBUG) Log.d(TAG, "Starting Spotlight call receiver service");
        startServiceAsCurrentUser(context, new Intent(context, CallReceiverService.class));
    }

    protected static void stopCallReceiverService(Context context) {
        if (DEBUG) Log.d(TAG, "Stopping Spotlight call receiver service");
        stopServiceAsCurrentUser(context, new Intent(context, CallReceiverService.class));
    }

    public static void startChargingService(Context context) {
        if (DEBUG) Log.d(TAG, "Starting Spotlight charging service");
        startServiceAsCurrentUser(context, new Intent(context, ChargingService.class));
    }

    protected static void stopChargingService(Context context) {
        if (DEBUG) Log.d(TAG, "Stopping Spotlight charging service");
        stopServiceAsCurrentUser(context, new Intent(context, ChargingService.class));
    }

    public static void startNotificationService(Context context) {
        if (DEBUG) Log.d(TAG, "Starting Spotlight notifs service");
        startServiceAsCurrentUser(context, new Intent(context, NotificationService.class));
    }

    protected static void stopNotificationService(Context context) {
        if (DEBUG) Log.d(TAG, "Stopping Spotlight notifs service");
        stopServiceAsCurrentUser(context, new Intent(context, NotificationService.class));
    }

    public static void startFlashlightService(Context context) {
        if (DEBUG) Log.d(TAG, "Starting Spotlight flashlight service");
        startServiceAsCurrentUser(context, new Intent(context, FlashlightService.class));
    }

    protected static void stopFlashlightService(Context context) {
        if (DEBUG) Log.d(TAG, "Stopping Spotlight flashlight service");
        stopServiceAsCurrentUser(context, new Intent(context, FlashlightService.class));
    }

    public static void startMusicService(Context context) {
        if (DEBUG) Log.d(TAG, "Starting Spotlight Music service");
        startServiceAsCurrentUser(context, new Intent(context, MusicService.class));
    }

    protected static void stopMusicService(Context context) {
        if (DEBUG) Log.d(TAG, "Stopping Spotlight Music service");
        stopServiceAsCurrentUser(context, new Intent(context, MusicService.class));
    }

    public static void checkSpotlightService(Context context) {
        PowerExemptionManager pem = context.getSystemService(PowerExemptionManager.class);
        pem.addToPermanentAllowList("net.typeblog.lunatic");

        // Self-grant notification access because why not :)
        NotificationManager nm = context.getSystemService(NotificationManager.class);
        try {
            Method setNotificationListenerAccessGranted = NotificationManager.class.getMethod("setNotificationListenerAccessGranted",
                    ComponentName.class, boolean.class, boolean.class);
            setNotificationListenerAccessGranted.invoke(nm, new ComponentName(context, NotificationService.class), true, true);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        AnimationManager animationManager = new AnimationManager(context);

        if (SettingsManager.isSpotlightEnabled(context)) {
            Constants.setBrightness(SettingsManager.getSpotlightBrightness(context));
            if (SettingsManager.isSpotlightChargingEnabled(context)) {
                startChargingService(context);
            } else {
                stopChargingService(context);
            }
            if (SettingsManager.isSpotlightCallEnabled(context)) {
                startCallReceiverService(context);
            } else {
                stopCallReceiverService(context);
            }
            if (SettingsManager.isSpotlightNotifsEnabled(context)) {
                startNotificationService(context);
            } else {
                stopNotificationService(context);
            }
            if (SettingsManager.isSpotlightFlashlightEnabled(context)) {
                startFlashlightService(context);
            } else {
                stopFlashlightService(context);
            }
            if (SettingsManager.isSpotlightMusicEnabled(context)) {
                startMusicService(context);
            } else {
                stopMusicService(context);
            }
        } else {
            stopChargingService(context);
            stopCallReceiverService(context);
            stopNotificationService(context);
            stopFlashlightService(context);
            stopMusicService(context);
        }
    }
}
