package net.typeblog.lunatic.Manager;

import android.os.RemoteException;

import vendor.mediatek.hardware.aguiledbelt.V1_0.IAguiLedBeltLight;

public class LEDManager {
    private static IAguiLedBeltLight sCtrl = null;

    private int mColor = 0xffffff;
    private int mBrightness = 100;
    private int mEnabledLeds = 0;

    private IAguiLedBeltLight ensureService() {
        if (sCtrl == null) {
            try {
                sCtrl = IAguiLedBeltLight.getService(true);
            } catch (RemoteException e) {
                throw new RuntimeException(e);
            }
        }

        return sCtrl;
    }

    private void syncState() {
        try {
            ensureService().setLedBeltAlwaysOnState(mEnabledLeds, mColor, mBrightness, false);
        } catch (RemoteException e) {
            throw new RuntimeException(e);
        }
    }

    public int getNumLEDs() {
        return 5;
    }

    public void enableLED(int ledno, boolean enabled) {
        if (enabled) {
            mEnabledLeds |= 1 << ledno;
        } else {
            mEnabledLeds &= ~(1 << ledno);
        }
        syncState();
    }

    public void setBrightness(int brightness) {
        if (brightness > 100) {
            brightness = 100;
        } else if (brightness < 0) {
            brightness = 0;
        }

        mBrightness = brightness;
        syncState();
    }

    public void setColor(int color) {
        mColor = color;
        syncState();
    }

    public void enableAllLEDs(boolean enable) {
        mEnabledLeds = enable ? 31 : 0;
        syncState();
    }

    public void setRawState(int ids, int color, int brightness) {
        if (brightness > 100) {
            brightness = 100;
        } else if (brightness < 0) {
            brightness = 0;
        }

        if (ids >= 32) {
            ids = 31;
        } else if (ids < 0) {
            ids = 0;
        }

        mEnabledLeds = ids;
        mColor = color;
        mBrightness = brightness;
        syncState();
    }
}
