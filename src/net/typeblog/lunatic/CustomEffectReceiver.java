package net.typeblog.lunatic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import net.typeblog.lunatic.Manager.LEDManager;
import net.typeblog.lunatic.Manager.StatusManager;

public class CustomEffectReceiver extends BroadcastReceiver {
    public static final String EFFECT_START = "net.typeblog.lunatic.EFFECT_START";
    public static final String EFFECT_SET = "net.typeblog.lunatic.EFFECT_SET";
    public static final String EFFECT_CLEAR = "net.typeblog.lunatic.EFFECT_CLEAR";
    public static final String EFFECT_END = "net.typeblog.lunatic.EFFECT_END";

    private LEDManager mManager = new LEDManager();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (EFFECT_START.equals(action)) {
            StatusManager.setCustomEffectActive(true);
        } else if (EFFECT_SET.equals(action)) {
            if (!StatusManager.isCustomEffectActive()) return;
            int ids = intent.getIntExtra("ids", 0);
            int color = intent.getIntExtra("color", 0xffffff);
            int brightness = intent.getIntExtra("brightness", 100);

            mManager.setRawState(ids, color, brightness);
        } else if (EFFECT_CLEAR.equals(action)) {
            if (!StatusManager.isCustomEffectActive()) return;
            mManager.enableAllLEDs(false);
        } else if (EFFECT_END.equals(action)) {
            if (!StatusManager.isCustomEffectActive()) return;
            mManager.enableAllLEDs(false);
            StatusManager.setCustomEffectActive(false);
        }
    }
}
